package HomeWorks.lesson6.task3;

public class Circle {
    int rad;


    public Circle() {

    }

    public void perimetr(int rad){
        this.rad = rad;
        System.out.println(Math.PI*2*rad);
    }

    public void area(int rad){
        this.rad = rad;
        System.out.println(Math.PI * rad*rad);
    }

}
