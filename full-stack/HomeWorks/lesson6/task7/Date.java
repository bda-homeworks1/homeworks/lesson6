package HomeWorks.lesson6.task7;

public class Date {
    int day;
    int month;
    int year;

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public void today(){
        System.out.println(this.day +"." + this.month + "." + this.year);
    }
}
