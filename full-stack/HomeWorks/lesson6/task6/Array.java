package HomeWorks.lesson6.task6;

public class Array {
    int[] arr;
    int index;


    public void minMax(int[] arr) {
        this.arr = arr;
        int maximum = arr[0];
        int minimum = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (maximum < arr[i]) {
                maximum = arr[i];
            }
            if (minimum > arr[i]) {
                minimum = arr[i];
            }
        }

        System.out.println("maksimum:" + maximum);
        System.out.println("minimum: " + minimum);
    }

    public void sort(int[] arr) {
        this.arr = arr;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[j] > arr[i]) {
                    int t = arr[i];
                    arr[i] = arr[j];
                    arr[j] = t;
                }

            }
        }
        System.out.println("sort olunmus array");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");

        }
    }

    public void removeIndex(int[] arr, int index) {
        this.arr = arr;
        this.index = index;
        int[] newArray = new int[arr.length - 1];
        int newI = 0;
        for (int i = 0; i < arr.length; i++) {
            if (i != index) {
                newArray[newI] = arr[i];
                newI++;
            }
        }

        System.out.println("teze array:");
        for (int i = 0; i < newArray.length; i++) {
            System.out.print(newArray[i] + " ");
        }
    }

}
